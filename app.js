const express = require("express");
const path = require("path");

const app = express();

// Middleware pour vérifier les heures ouvrables
const workingHoursMiddleware = (req, res, next) => {
  const now = new Date();
  const day = now.getDay(); // 0 (dimanche) à 6 (samedi)
  const hour = now.getHours();

  const isOpen = day >= 1 && day <= 5 && hour >= 9 && hour < 17;
  if (isOpen) {
    next();
  } else {
    res.send("<h1>Site fermé. Veuillez revenir pendant les heures ouvrables (Lundi à Vendredi, 9h-17h).</h1>");
  }
};

// Appliquer le middleware
app.use(workingHoursMiddleware);

// Servir les fichiers statiques (CSS)
app.use(express.static(path.join(__dirname, "")));

// Routes
app.get("/", (req, res) => res.sendFile(path.join(__dirname, "index.html")));
app.get("/nos-services", (req, res) => res.sendFile(path.join(__dirname, "watch.html")));
app.get("/contactez-nous", (req, res) => res.sendFile(path.join(__dirname, "contact.html")));

app.get("/contactez-nous", (req, res) => res.sendFile(path.join(__dirname, "about.html")));

// Démarrer le serveur
const PORT = 3000;
app.listen(PORT, () => console.log(`Serveur démarré sur http://localhost:${PORT}`));
